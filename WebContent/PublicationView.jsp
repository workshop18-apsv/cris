<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Publication</title>
</head>
<body>
<table>
<tr>
<th>Id</th><th>title</th><th>Publication Name</th><th>Publication Date</th><th>Authors</th><th>Cite Count</th> 
</tr>
        <tr>
           <td> ${publication.id} </td>
           <td>${publication.title}</td>
           <td>${publication.publicationName}</td>
           <td>${publication.publicationDate}</td>
           <td>${publication.authors}</td>
           <td>${publication.citeCount}</td>
        </tr>
</table>
<form action="UpdateCitationsAPIServlet" method="get">
        <button type="submit">Update Cite Count</button>
</form>

</body>
</html>