<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Admin</title>
</head>
<body>

<form action="CreateResearcherServlet" method="post">
        <input type="text" name="userId" placeholder="User Id">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="lastname" placeholder="Last name">
        <input type="text" name="email" placeholder="Email">
        <button type="submit">Create researcher</button>
</form>

<form action="CreatePublicationServlet" method="post">
        <input type="text" name="publicationId" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="publicationName" placeholder="Publication Name">
        <input type="text" name="publicationDate" placeholder="Publication Date">
        <input type="text" name="authors" placeholder="Authors">
        <button type="submit">Create publication</button>
</form>

</body>
</html>