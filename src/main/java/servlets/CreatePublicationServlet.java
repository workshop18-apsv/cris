package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import model.Publication;
import model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher researcherAdmin = (Researcher) request.getSession().getAttribute("user");
		String login = researcherAdmin.getId();		
	
		Client client = ClientBuilder.newClient(new ClientConfig()); 

		if(login.equals("root")) {
			String publicationId = (String) request.getParameter("publicationId"); 
			String publicationName = (String) request.getParameter("publicationName");
			String publicationDate = (String) request.getParameter("publicationDate");
			String authors = (String) request.getParameter("authors");
			Publication publication = new Publication();
			publication.setId(publicationId);
			publication.setPublicationName(publicationName);
			publication.setPublicationDate(publicationDate);
			publication.setAuthors(authors);

			
			client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications")
			.request()
			.post(Entity.entity(publication, MediaType.APPLICATION_JSON), Response.class);
		

	
	        response.sendRedirect(request.getContextPath() + "/AdminServlet");
	        return;
			
			
		}
		else {
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	}

}
