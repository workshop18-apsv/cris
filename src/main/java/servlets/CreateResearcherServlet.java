package servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import model.Researcher;

@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher researcherAdmin = (Researcher) request.getSession().getAttribute("user");
		String login = researcherAdmin.getId();		
	
		Client client = ClientBuilder.newClient(new ClientConfig()); 

		if(login.equals("root")) {
			
			List<Researcher> researchersList  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers") 
					.request().accept(MediaType.APPLICATION_JSON) 
					.get(new GenericType<List<Researcher>>() {}); 
			
			String userId = (String) request.getParameter("userId");
			for(Researcher ri : researchersList) {
				if(userId.equals(ri.getId()))
					return;
			}
			
			String name = (String) request.getParameter("name");
			String lastname = (String) request.getParameter("lastname");
			String email = (String) request.getParameter("email");
			Researcher researcher = new Researcher();
			researcher.setId(userId);
			researcher.setName(name);
			researcher.setLastname(lastname);
			researcher.setEmail(email);
			
			client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers")
				.request()
				.post(Entity.entity(researcher, MediaType.APPLICATION_JSON), Response.class);

	        response.sendRedirect(request.getContextPath() + "/AdminServlet");
	        return;
			
			
		}
		else {
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
			
			
		
	
		//client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers") 
		
		/*
		Researcher researcher  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers") 
				.request().accept(MediaType.APPLICATION_JSON) 
				.get(Researcher.class);
		request.setAttribute ("publication", publication);
		getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);*/
	}

}
