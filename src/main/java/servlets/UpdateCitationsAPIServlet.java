package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import model.Publication;
import model.Researcher;

@WebServlet("/UpdateCitationsAPIServlet")
public class UpdateCitationsAPIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = (String) request.getParameter("id");
		Client client = ClientBuilder.newClient(new ClientConfig()); 
		request.setAttribute ("id", id);
		response.sendRedirect(request.getContextPath() + "/PublicationServlet");
		
		
		client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/"+id+"/UpdateCiteNumber");
		
		getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);
		
		Publication publication  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/"+id) 
				.request().accept(MediaType.APPLICATION_JSON) 
				.get(Publication.class);
		request.setAttribute ("publication", publication);
		getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response); 
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
