package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import model.Researcher;

@WebServlet("/ResearchersListServlet") //ruta de entrada
public class ResearchersListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Client client = ClientBuilder.newClient(new ClientConfig()); //cliente rest
		List<Researcher> researcherslist  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers") //devuelve lista de investigadore
				.request().accept(MediaType.APPLICATION_JSON) //la lista esta en json
				.get(new GenericType<List<Researcher>>() {}); //aqui la convertimos a arraylist
		request.setAttribute ("researcherslist", researcherslist); //disponible hasta que se termine de ejecutar la petición
		getServletContext().getRequestDispatcher("/ResearchersListView.jsp").forward(request, response); //le paso los dato a la vista .jsp con forward pasa el atributo de request
	}
//no nos va a inteneresar do post por eso lo ponen a doget
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
